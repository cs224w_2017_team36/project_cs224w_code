import gzip
import os
import glob
import csv
from xml.etree import ElementTree as ET
from io import StringIO
from bs4 import BeautifulSoup as BS
#import timeit

# Define the location of the source file(s)
from Src.settings import INPUT_DATA_ARNU, OUTPUT_DATA_XML
INPUT_DATA_ARNU_MONTH = os.path.join(INPUT_DATA_ARNU, '08')

def func_generate_csv(filepath, targetpath):

    write_header = ["service","stopservicecode","stopcode", "departure","departuretimedelay","type"]

    input_filename = str(filepath.split('\\')[-1])
    targetpath_file = str(input_filename.split('.')[0]) +"." + str(input_filename.split('.')[2]) + '.csv'

    print(targetpath_file)

    with gzip.open(filepath, 'rb') as z:
        # Open het arnu-bestand
        file_content = z.read().decode()

        s = StringIO(file_content)

        with open(os.path.join(targetpath, targetpath_file), 'w', newline='') as f:
            c = csv.writer(f, delimiter=',') # just added, test: , extrasaction='ignore'
            c.writerow(write_header)
            all_lines = []
            for line in s:
                # every row is a single xml
                root = BS(line, "lxml")
                tree = ET.parse(StringIO(root.prettify()))
                for parent in tree.iter('serviceinfo'):
                    sc = parent.get('servicetype')
                    sl = parent.findtext("./servicecode").strip()
                    for stop in parent.iter('stop'):
                       if stop.findtext("./departure", default="empty").strip() != "empty":
                           write_line = [sc, sl, stop.findtext("./stopcode").strip()
                               , stop.findtext("./departure", default="empty").strip(),
                                         stop.findtext("./departuretimedelay", default="empty").strip(),"dep"]
                           if ''.join(write_line) in all_lines:
                               pass
                           else:
                               all_lines.append(''.join(write_line))
                               c.writerow(write_line)
                       elif stop.findtext("./arrival", default="empty").strip() != "empty":
                           write_line = [sc, sl, stop.findtext("./stopcode").strip()
                               , stop.findtext("./arrival", default="empty").strip(),
                                         stop.findtext("./arrivaltimedelay", default="empty").strip(),"arr"]
                           if ''.join(write_line) in all_lines:
                               pass
                           else:
                               all_lines.append(''.join(write_line))
                               c.writerow(write_line)

                       else:
                           pass


#date = "2017-04-01"
#filepath = INPUT_DATA_ARNU + r"\arnu-ritinfo.log." + date + ".gz"

#start_time = timeit.default_timer()

#func_generate_csv(filepath, OUTPUT_DATA_XML)

#print("time balance checker %s" % (timeit.default_timer() - start_time))

for fn in glob.glob(os.path.join(INPUT_DATA_ARNU_MONTH, '*.gz')):
    func_generate_csv(fn, OUTPUT_DATA_XML)

#    exit()

