import snap
import csv
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import math
from collections import OrderedDict
import ReversedTopology

StopsFileName = 'C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/stops_with_id_1112.csv'
# basically we need to add one more column (numeric id) in Stop.csv file 
DelayFileName = 'C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/Delayinfo_aggregated_april_and_may_mf_quarterhour_11_12.csv'

TrainTopologyGraph ="C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/Train_network_full_data_store_mf_simplified_11_12_WithID_2.csv"
OutPutGraphFileNameWithIDAndCoeff3 ="C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/Train_network_full_data_store_mf_simplified_11_12_WithIDAndCoeff3_5minsOrMore_fiveDaysAvg.csv"

OutputCoeffMapCoeff3 ="C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/Train_network_full_data_store_mf_simplified_11_12_CoeffMap_5minsOrMore_fiveDaysAvg.csv"

SrcColIdStr = "SrcColId"
DstColIdStr = 'DstColId'
DateStr = "date"
StopNameStr = "\xef\xbb\xbfnode_id";
CoeffStr = "coeff"
DelayBuckets = ["TOTAL_nr_of_delays", "TOTAL_nr_of_stops", "nr_0min_delays", "nr_1min_delays", "nr_2min_delays", "nr_3min_delays", "nr_4min_delays", "nr_5min_delays", "nr_6min_delays", "nr_7min_delays", "nr_8min_delays", "nr_9min_delays", "nr_10min_delays", "nr_11min_delays", "nr_12min_delays", "nr_13min_delays", "nr_14min_delays", "nr_15min_delays", "nr_16min_delays", "nr_17min_delays", "nr_18min_delays", "nr_19min_delays", "nr_20min_to_24min_delays", "nr_25min_to_29min_delays", "nr_30min_to_34min_delays", "nr_35min_to_39min_delays", "nr_40min_to_44min_delays", "nr_45min_to_49min_delays", "nr_50min_and_plus_delays"]

TotalNumberOfStopsStr = "TOTAL_nr_of_stops"
DelayProbabilityDistribution = "DelayProbabilityDistribution"

# date is the specific date we are looking at
def DelayAggregationOnGivenDateOnDate(delayFileName, dates):
	delaysPerNodeQuater = {};
	with open(delayFileName, 'rb') as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			if (row[DateStr] in dates):
				##print row[DateStr]
				if (row[StopNameStr] in delaysPerNodeQuater.keys()):
					for idx in range(len(DelayBuckets)):
						delaysPerNodeQuater[row[StopNameStr]][idx] += int(row[DelayBuckets[idx]])
				else:
					delaySignature = [0] * len(DelayBuckets)
					for idx in range(len(DelayBuckets)):
						delaySignature[idx] = int(row[DelayBuckets[idx]]);
					delaysPerNodeQuater[row[StopNameStr]] = delaySignature;

	return delaysPerNodeQuater

def GetDelayRatio(aggregatedEvents):
	return [aggregatedEvents[key][0] * 1.0 /aggregatedEvents[key][1]  for key in aggregatedEvents.keys()]
	
def PlotDelaySignature(aggregatedEvents, graphName, xLabel, yLabel):
	signature = [0] * (len(DelayBuckets))
	for key in aggregatedEvents.keys():
		for idx in range(0, (len(aggregatedEvents[key]))):
			signature[idx] += int(aggregatedEvents[key][idx]);
		
	signature20plus = sum(signature[22:]);
	signature = signature[1:22];
	signature.append(signature20plus);
	buckets = ["All", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20+"]
	y_pos = np.arange(len(signature))
	y_pos = 2* y_pos ;
	plt.bar(y_pos, signature, align='center', alpha=0.5)
	plt.xticks(y_pos, buckets)
	plt.xlabel(xLabel)
	plt.ylabel(yLabel)
	plt.yscale('log')
	plt.title("{0} Distribution".format(xLabel))
	#plt.axis([40, 160, 0, 0.03])
	plt.grid(True)
	plt.savefig("C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/res/{0}_fiveDaysAvg.png".format(graphName));
	# plt.show()
	plt.close()
	
def plotDistribution(coeff, graphName, xLabel, yLabel):
	# the histogram of the data
	# print len(coeff)
	coeff = [value for value in coeff if is_number(value)]
	n, bins, patches = plt.hist(coeff, 50, normed=1, facecolor='green', alpha=0.75)
	plt.xlabel(xLabel)
	plt.ylabel(yLabel)
	plt.title("{0} Distribution".format(xLabel))
	#plt.axis([40, 160, 0, 0.03])
	plt.grid(True)
	plt.savefig("C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/res/{0}_fiveDaysAvg.png".format(graphName));
	# plt.show()
	plt.close()
	
def calculateCorrelations(aggregatedEvents, plan):
	reOrganizedBuckets = {};
	coeff = {}; # diction of dictionary, keys are two nodes values are the correlation values.
	if plan == 1: # plan correlation including all data
		for key in aggregatedEvents.keys():
			reOrganizedBuckets[key] = aggregatedEvents[key][1:]; # get ride of total number of delay
			# meanSquareRoot= math.sqrt(1.0 * sum([reOrganizedBuckets[key][idx]**2 for idx in range(1, len(reOrganizedBuckets[key]))])/(len(reOrganizedBuckets[key]) - 1))
			# reOrganizedBuckets[key][0] = meanSquareRoot
	elif plan == 2: # including all data use log (x + 1)
		for key in aggregatedEvents.keys():
			reOrganizedBuckets[key] = [math.log(1 + aggregatedEvents[key][idx]) for idx in range(1, len(aggregatedEvents[key]))]; # get ride of total number of delay, log( 1 + y)
			# meanSquareRoot= math.sqrt(1.0 * sum([reOrganizedBuckets[key][idx]**2 for idx in range(1, len(reOrganizedBuckets[key]))])/(len(reOrganizedBuckets[key]) - 1))
			# reOrganizedBuckets[key][0] = meanSquareRoot
	elif plan == 3: # exclude the ones without delay 
		for key in aggregatedEvents.keys():
			reOrganizedBuckets[key] = aggregatedEvents[key][3:]; # get ride of the zero delays.
			# meanSquareRoot= math.sqrt(1.0 * sum([reOrganizedBuckets[key][idx]**2 for idx in range(1, len(reOrganizedBuckets[key]))])/(len(reOrganizedBuckets[key]) - 1))
			# reOrganizedBuckets[key][0] = meanSquareRoot
	elif plan == 4: # exclude events within 5 mins of delay
		for key in aggregatedEvents.keys():
			reOrganizedBuckets[key] = aggregatedEvents[key][7:]; # get ride of the zero delays.
			# meanSquareRoot= math.sqrt(1.0 * sum([reOrganizedBuckets[key][idx]**2 for idx in range(1, len(reOrganizedBuckets[key]))])/(len(reOrganizedBuckets[key]) - 1))
			# reOrganizedBuckets[key][0] = meanSquareRoot
	else:
		# unsupported scenario
		return
	keys = reOrganizedBuckets.keys();
	for idx in range(len(keys)):
		for idy in range(len(keys)):
			if (keys[idx] not in coeff.keys()):
				coeff[keys[idx]] = {};
			coeff[keys[idx]][keys[idy]] = calculateCorrelationBase(reOrganizedBuckets[keys[idx]], reOrganizedBuckets[keys[idy]]);
	return coeff;

def calculateCosinSimmilarity(aggregatedEvents):
	reOrganizedBuckets = {};
	for key in aggregatedEvents.keys():
		reOrganizedBuckets[key] = aggregatedEvents[key][2:]* 1.0/aggregatedEvents[key][1]; # get ride of the zero delays.
	coeff = {}; # diction of dictionary, keys are two nodes values are the correlation values.
	keys = reOrganizedBuckets.keys();
	for idx in range(len(keys)):
		for idy in range(len(keys)):
			if (keys[idx] not in coeff.keys()):
				coeff[keys[idx]] = {};
			coeff[keys[idx]][keys[idy]] = calculateCorrelationBase(reOrganizedBuckets[keys[idx]], reOrganizedBuckets[keys[idy]]);
	return coeff;
	
def calculateCorrelationBase(array1, array2):
	# product = 0;
	# for idx in range(1, len(array1)):
		# product += array1[idx] * array2[idx];
	# return product * 1.0 / array1[0] / array2[0];
	corr = np.corrcoef(array1, array2);
	return corr[0][1]

def printTopKeyNodes(coeff, k):
	sortedNodes = sortCoeff(coeff);
	for key, value in sortedNodes.items():
		k -= 1; 
		if k < 0: break;
		print "(Name: %s, NodeId: %s, coeff: %f)" % (key,nodeNameIdMappings[key.upper()], value);
		
def sortCoeff(coeff):
	return OrderedDict(sorted(coeff.items(), key=lambda x: -x[1]))

		
def plotCorrelationDistribution(coeffMap):
	nodes = coeffMap.keys();
	k = 10
	m = 10
	print "1 avg all coefficients"
	coeff1 = {}
	for node in nodes:
		coeff1[node] = np.mean([value for value in coeffMap[node].values() if is_number(value)]);
	plotDistribution(coeff1.values(), "Avg_correlation2_all_nodes", "Avg Correlations of all nodes", "count")
	printTopKeyNodes(coeff1, k)
	# print "2 avg top 10 coefficients"
	# coeff2 = {}
	# for node in nodes:
		# coeffList = coeffMap[node].values()
		# coeffList.sort(reverse = True)
		# coeff2[node] = np.mean([value for value in coeffList[1:m + 1]] if not math.isnan(value));  # 0 is itself
	# plotDistribution(coeff2.values(), "Avg_correlation2_top_{0}_nodes".format(m), "Avg Correlation2 of top {0} nodes".format(m), "count")
	# printTopKeyNodes(coeff2, k)

def plotCorrelationDistributionWithGraph(correlationCoeff, Graph, NodeIdNameMappings):
	coeff3AvgWithGraph = {}
	#print NodeIdNameMappings;
	#print correlationCoeff.keys()
	for node in Graph.Nodes():
		degree = node.GetDeg()
		coeffAllNeibours = []
		currentNodeName =NodeIdNameMappings[node.GetId()].lower();
		if currentNodeName in correlationCoeff.keys():
			for nbr in xrange(degree):
				neighborID = node.GetNbrNId(nbr)
				neighbourName = NodeIdNameMappings[neighborID].lower()
				if neighbourName in correlationCoeff[neighbourName].keys():
					coeffTmp = correlationCoeff[currentNodeName][neighbourName];
					if is_number(coeffTmp): 
						coeffAllNeibours.append(coeffTmp);
				else:
					print "missing Node" + NodeIdNameMappings[neighborID]
				if (len(coeffAllNeibours) > 0):
					coeff3AvgWithGraph[currentNodeName] = sum(coeffAllNeibours)/len(coeffAllNeibours);
		else:
			print "missing Node" + NodeIdNameMappings[node.GetId()]	
	np.save('coeff3AvgWithGraph_5days.npy', coeff3AvgWithGraph)
	plotDistribution(coeff3AvgWithGraph.values(), "Avg_correlation3_allNeighbours", "Avg Correlations of all nodes", "count")
	printTopKeyNodes(coeff3AvgWithGraph, 10)

def updateTransitNetworkWithCoeff(graphFile, CoeffMappings, outputFileName):
	graphEdges = [];
	fieldNames = ["\xef\xbb\xbfRoute_ID", "Node1", "Node2", SrcColIdStr, DstColIdStr, CoeffStr]
	coeffs=[]
	print CoeffMappings.keys()
	with open(graphFile, 'rb') as csvfile:
		reader = csv.DictReader(csvfile, delimiter ='\t');
		for row in reader:
			print(row)
			srcNode, dstNode = row["Node1"].lower(), row["Node2"].lower();
			if (srcNode in CoeffMappings.keys()):
				if (dstNode in CoeffMappings.keys()):
					row[CoeffStr] = CoeffMappings[srcNode][dstNode];
					graphEdges.append(row);
					coeffs.append(row[CoeffStr])
				else : print "missing Node: %s" % dstNode;
			else :  print "missing Node: %s" % srcNode;
	with open(outputFileName,"wb") as csvfile:
		writer=csv.DictWriter(csvfile, fieldnames = fieldNames, delimiter='\t');
		writer.writeheader();
		writer.writerows(graphEdges)	
	plotDistribution(coeffs, "Coeff3DistbetweenTwoNodeOfAllEdges", "Correlation of connected nodes", "count")
	
	
def outputCoeffMaps(CoeffMappings, outputFileName):
	graphEdges = [];
	fieldNames = ["Node1", "Node2", CoeffStr]
	coeffPairs=[]
	keys = CoeffMappings.keys()
	for i in range(len(keys) - 1):
		for j in range(i + 1, len(keys)):
			if is_number(CoeffMappings[keys[i]][keys[j]]):
				row = {}
				row["Node1"],row["Node2"] = keys[i],keys[j]; 
				row[CoeffStr] = CoeffMappings[keys[i]][keys[j]]
				coeffPairs.append(row);
	with open(outputFileName,"wb") as csvfile:
		writer=csv.DictWriter(csvfile, fieldnames = fieldNames, delimiter='\t');
		writer.writeheader();
		writer.writerows(coeffPairs)	
	#plotDistribution(coeffs, "Coeff3DistbetweenTwoNodeOfAllEdges", "Correlation of connected nodes", "count")

def getAvgCoeff(coeffDatesMap, dates):
	coeff = coeffDatesMap["31/5/2017"];
	stations = coeff.keys();
	for station1 in stations:
		for station2 in stations:
			coeffs = [];
			for date in dates:
				if (station1 in coeffDatesMap[date].keys() and station2 in coeffDatesMap[date].keys()):
					if (is_number(coeffDatesMap[date][station1][station2])):
						coeffs.append(coeffDatesMap[date][station1][station2])
			if (len(coeffs) > 0) :
				coeff[station1][station2] = sum(coeffs) / len(coeffs);
	return coeff;
	
def is_number(s):
    try:
        return not math.isnan(float(s))
    except ValueError:
        return False
date = "31/5/2017"
#dates = {"31/5/2017"};
dates = {"25/5/2017", "26/5/2017", "29/5/2017", "30/5/2017", "31/5/2017"}
print DelayBuckets[0:3]
tmp = date.split('/');
tmp.reverse();
dateForOutput = "_".join(tmp);
aggregatedevents = DelayAggregationOnGivenDateOnDate(DelayFileName,dates);
PlotDelaySignature(aggregatedevents, "delaysignatureforallnodes", "delay time", "count");
coeff = GetDelayRatio(aggregatedevents)
plotDistribution(coeff, DelayProbabilityDistribution + "_" + dateForOutput, "delay probability", "ratio")
node = "had"
coeffSeveryDay = {}
for date in dates:
	aggregatedevents = DelayAggregationOnGivenDateOnDate(DelayFileName,[date]);
	coeffSeveryDay[date] = calculateCorrelations(aggregatedevents, 4)
np.save('coeffsEveryDay_coeff3_5minsOrMore.npy', coeffSeveryDay) 
coeffSeveryDay = np.load('coeffsEveryDay_coeff3_5minsOrMore.npy').item()
coeffAvg = getAvgCoeff(coeffSeveryDay, dates);

#print coeffAvg
plotDistribution(coeffAvg, DelayProbabilityDistribution + "_" + dateForOutput, "Delay Probability", "ratio")
nodeNameIdMappings = np.load('nameIdMappings.npy').item()
nodeIdNameMappings = {int(v): k for k, v in nodeNameIdMappings.items()}
plotCorrelationDistribution(coeffAvg)

Graph = snap.LoadEdgeList(snap.PNGraph, TrainTopologyGraph, 3, 4);

plotCorrelationDistributionWithGraph(coeffAvg, Graph, nodeIdNameMappings);
updateTransitNetworkWithCoeff(TrainTopologyGraph, coeffAvg, OutPutGraphFileNameWithIDAndCoeff3);
outputCoeffMaps(coeffAvg, OutputCoeffMapCoeff3);

# # plan Coeff including 0min delay:
# #correlationCoeff1 = calculateCorrelations(aggregatedEvents, 1)
# #np.save('correlationCoeff1.npy', correlationCoeff1) 
# correlationCoeff1 = np.load('correlationCoeff1.npy').item() 
# #plotDistribution(correlationCoeff1[node].values(), "correlation1_For_Stop_" + node, "Correlation for " + node, "count")

# # log
# # correlationCoeff2 = calculateCorrelations(aggregatedEvents, 2)
# # np.save('correlationCoeff2.npy', correlationCoeff2) 
# correlationCoeff2 = np.load('correlationCoeff2.npy').item()
# #plotDistribution(correlationCoeff2[node].values(), "correlation2_For_Stop_" + node, "Correlation for " + node, "count")

#correlation without 0 minutes delay
correlationCoeff3 = calculateCorrelations(aggregatedEvents, 3)
np.save('correlationCoeff3_oneDay.npy', correlationCoeff3) 
correlationCoeff3 = np.load('correlationCoeff3_oneDay.npy').item()
plotDistribution(correlationCoeff3[node].values(), "correlation3_For_Stop_" + node, "Correlation for " + node, "count")

# plot for all nodes
# for node in correlationCoeff1.keys():
	# plotDistribution(correlationCoeff1[node].values(), "correlation1_For_Stop_" + node, "Correlation for " + node, "count")
	# plotDistribution(correlationCoeff2[node].values(), "correlation2_For_Stop_" + node, "Correlation for " + node, "count")
	# plotDistribution(correlationCoeff3[node].values(), "correlation3_For_Stop_" + node, "Correlation for " + node, "count")

nodeNameIdMappings = np.load('nameIdMappings.npy').item()
nodeIdNameMappings = {int(v): k for k, v in nodeNameIdMappings.items()}
plotCorrelationDistribution(correlationCoeff3)

Graph = snap.LoadEdgeList(snap.PNGraph, TrainTopologyGraph, 3, 4);

plotCorrelationDistributionWithGraph(correlationCoeff3, Graph, nodeIdNameMappings);
updateTransitNetworkWithCoeff(TrainTopologyGraph, correlationCoeff3, OutPutGraphFileNameWithIDAndCoeff3);


