import snap
import csv
import numpy as np

StopsFileName = 'C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/stops_with_id_1201.csv'
# basically we need to add one more column (numeric id) in Stop.csv file 
TrafficTopologyFileName ="C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/Train_network_full_data_store_mf_simplified_12_01.csv"
OutPutGraphFileNameWithID ="C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/Train_network_full_data_store_mf_simplified_12_01_WithID_2.csv"
OutputGraphWithReversedTopologyFileName = "C:/Users/liyun/Documents/cs224w/project/repo/project_cs224w_code/data/Train_network_full_data_store_mf_simplified_12_01_reversedTopology.csv"

SrcColIdStr = "SrcColId"
DstColIdStr = 'DstColId'

def readStopNameIdMapping(stopFileName):
	nameIdMappings = {};
	with open(stopFileName, 'rb') as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			nameIdMappings[row["NodeID"]] = row["StopID"];
	return nameIdMappings

# add the numeric column to the edge list
def updateTransitNetworkWithNumericId(graphFile, nameIdMappings, outputFileName):
	graphEdges = [];
	sets = set([]);
	setsIds = set([])
	fieldNames = ["\xef\xbb\xbfRoute_ID", "Node1", "Node2", "Node1_name", "Node2_name", SrcColIdStr, DstColIdStr]
	with open(graphFile, 'rb') as csvfile:
		reader = csv.DictReader(csvfile);
		for row in reader:
			#print(row)
			srcNode, dstNode = row["Node1"], row["Node2"];
			if (srcNode in nameIdMappings.keys()):
				if (dstNode in nameIdMappings.keys()):
					sets.add(srcNode)
					sets.add(dstNode)
					setsIds.add(nameIdMappings[srcNode])
					setsIds.add(nameIdMappings[dstNode])
					row[SrcColIdStr], row[DstColIdStr] = nameIdMappings[srcNode], nameIdMappings[dstNode];
					graphEdges.append(row);
				else : print "missing Node: %s" % dstNode;
			else :  print "missing Node: %s" % srcNode;
	print fieldNames;
	print len(sets);
	print sets;
	print len(setsIds);
	print setsIds;
	setsIds
	with open(outputFileName,"wb") as csvfile:
		writer=csv.DictWriter(csvfile, fieldnames = fieldNames, delimiter='\t');
		writer.writeheader();
		writer.writerows(graphEdges)

def populateAlledges(stopRouteMappings, fieldNames):
	graphEdges = [];
	for eachStopId in stopRouteMappings.keys():
		routeSetsPassingThisStop = list(stopRouteMappings[eachStopId]);
		for srcIndex in range(len(routeSetsPassingThisStop) - 1):
			for dstIndex in range(srcIndex + 1, len(routeSetsPassingThisStop)):
				edge = {}
				edge[fieldNames[0]] = routeSetsPassingThisStop[srcIndex]; # RouteID1 
				edge[fieldNames[1]] = routeSetsPassingThisStop[dstIndex]; # RouteID2
				edge[fieldNames[2]] = eachStopId;  # IntersectionStopId
				graphEdges.append(edge)
	return graphEdges;
	
def createReverseGraphTopology(graphFile,outputFileName, nameIdMappings):
	fieldNames = ["RouteID1", "RouteID2", "IntersectionStopId"]
	stopRouteMappings = {}; # stopId: set of routeId that have stop with this stopId 
	with open(graphFile, 'rb') as csvfile:
		reader = csv.DictReader(csvfile);
		for row in reader:
			#print(row)
			routeId = int(row["\xef\xbb\xbfRoute_ID"]);
			#print "RouteId %d" %(routeId)
			srcNode, dstNode = row["Node1"], row["Node2"];
			if (srcNode in nameIdMappings.keys()):
				if (dstNode in nameIdMappings.keys()):
					srcNodeId = nameIdMappings[srcNode];
					if (srcNodeId in stopRouteMappings.keys()): stopRouteMappings[srcNodeId].add(int(routeId));
					else: stopRouteMappings[srcNodeId]= set([int(routeId)]);
					dstNodeId = nameIdMappings[dstNode];
					if (dstNodeId in stopRouteMappings.keys()): stopRouteMappings[dstNodeId].add(routeId);
					else: stopRouteMappings[dstNodeId]= set([int(routeId)]);
				else : print "missing Node: %s" % dstNode;
			else :  print "missing Node: %s" % srcNode;
	print stopRouteMappings;
	graphEdges = populateAlledges(stopRouteMappings, fieldNames);
	print fieldNames;
	with open(outputFileName,"wb") as csvfile:
		writer=csv.DictWriter(csvfile, fieldnames = fieldNames, delimiter='\t');
		writer.writeheader();
		writer.writerows(graphEdges)

		
nameIdMappings = readStopNameIdMapping(StopsFileName)
np.save('nameIdMappings_12_01.npy', nameIdMappings) 
updateTransitNetworkWithNumericId(TrafficTopologyFileName, nameIdMappings, OutPutGraphFileNameWithID)
createReverseGraphTopology(TrafficTopologyFileName, OutputGraphWithReversedTopologyFileName, nameIdMappings)

			

			