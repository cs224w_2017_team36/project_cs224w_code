
# coding: utf-8

# In[105]:


import snap
#import matplotlib
#matplotlib.use('QT4Agg') #Problem with 64 bit installation, to be solved later
import matplotlib.pyplot as plt
import random
from scipy.stats import chi2_contingency
import numpy as np
import pandas as pd


# In[231]:


df = pd.read_csv('CS224W/project_cs224w_code/data/Train_network_full_data_store_mf_simplified_11_12_WithID.csv', delimiter="\t")
#print len(df)
#print df[1:5]
#print df.iloc[0:5, 0:2]
df_route_length = df.groupby("Route_ID")['Node1'].count()

print len(df), df_route_length[0:5].mean()




# In[92]:


# load the train network as undeirect graph
train_un = snap.LoadEdgeList(snap.PUNGraph, 'CS224W/project_cs224w_code/data/Train_network_full_data_store_mf_simplified_11_12_WithID.csv' , 3, 4)


# In[141]:


print train_un.GetNodes()  # 251
print train_un.GetEdges()  # 352
random_un = snap.GenRndGnm(snap.PUNGraph, train_un.GetNodes(), train_un.GetEdges())  # create random network with same # of nodes and edges
print random_un.GetNodes()
print random_un.GetEdges()


# In[214]:


def Get_nodes_degree(graph):
    """
    Function to return reverse ordering node_id, and node_degree 
    """
    DegToCntV = snap.TIntPrV()
    snap.GetNodeInDegV(graph, DegToCntV)
    DegToCntV_s = sorted(DegToCntV, key = lambda item: item.GetVal2(), reverse = True)
    return ([(item.GetVal1(), item.GetVal2()) for item in DegToCntV_s])

def Get_nodes_betweenness(graph):
    """
    Function to return reverse ordering node_id, and node_centricity
    """
    Nodes_betweeness = snap.TIntFltH()
    Edges = snap.TIntPrFltH()
    snap.GetBetweennessCentr(graph, Nodes_betweeness, Edges, 1.0)
    Nodes_betweenes_s = sorted(Nodes_betweeness, key = lambda key: Nodes_betweeness[key], reverse = True)
    return ([(item, Nodes_betweeness[item]) for item in Nodes_betweenes_s])
    
def Get_nodes_ClusterCoefficient(Graph):
    """
    Function to return reverse ordering node_id, and cluster_coeff
    """
    coeff = []
    for node in Graph.Nodes():
        coeff.append((node.GetId(),snap.GetNodeClustCf(Graph, node.GetId())))
    coeff_s = sorted(coeff, key = lambda item: item[1], reverse = True)
    return coeff_s
              
    
def Create_hist_nodes_degree(nd_inp):
    output = {}
    for item in nd_inp:
        if item[1] in output:
            output[item[1]] += 1
        else:
            output[item[1]] = 1
    x = [key for key in output]
    y = [output[key] for key in output]
    
    return x, y 

def SIR(Graph, nodes_I, beta, delta):
    """
    Function to perform the infection process using SIR model. 

    :param - Graph: snap.PUNGraph object representing an undirected graph
    :param - infaction rate and recovery rate
    :param - nodes_I, a set of nodes
    
    return: fraction of nodes infected 
    """

    nodes_all = []
    for NI in Graph.Nodes():
        nodes_all.append(NI.GetId())
    nodes_R = set([])
    nodes_S = set(nodes_all) - nodes_I
    #print "Initial infected ", nodes_I
    
    while(len(nodes_I) !=0):
        #print "infected nodes are ", len(nodes_I)
        #print nodes_I
        if (len(nodes_I) ==len(nodes_all)):
            break
        S_s, I_s, J_s, R_s = [],[],[],[]
        for u in nodes_all:
            #print u, nodes_I
            if u in nodes_S:
                for NI in Graph.GetNI(u).GetOutEdges():
                    if (NI in nodes_I):
                        ran_number = random.random()
                        if ran_number < beta:
                            S_s.append(u)
                            I_s.append(u)
                            break
            elif u in nodes_I:
                ran_number = random.random()
                #print "u in infected node", u, ran_number
                if ran_number < delta:
                    J_s.append(u)
                    R_s.append(u)
        nodes_S = nodes_S - set(S_s)
        nodes_I = (nodes_I | set(I_s)) - set(J_s)
        nodes_R = nodes_R | set(R_s)
    
    return float(len(nodes_R))/len(nodes_all)

def plotClusterCoefficientDistribution(Graph, graphName):
        coeff = []
        for node in Graph.Nodes():
                coeff.append(snap.GetNodeClustCf(Graph, node.GetId()));
        # the histogram of the data
        n, bins, patches = plt.hist(coeff, 50, normed=1, facecolor='green', alpha=0.75)
        plt.xlabel('Clust Coefficient')
        plt.ylabel('Count')
        plt.title("clust_Coefficients_Distribution_for_{0}".format(graphName))
        #plt.axis([40, 160, 0, 0.03])
        plt.grid(True)
        #plt.savefig("../res/charac/cluster_Coefficient_distribution_{0}_graph_in_log_scale.png".format(graphName));
        plt.show()
        return n, bins

    
    
def plotClusterCoefficientDistribution_v2(Graphs, graphName):
    for i in range(len(Graphs)):
        Graph = Graphs[i]   
        coeff = []
        for node in Graph.Nodes():
                coeff.append(snap.GetNodeClustCf(Graph, node.GetId()));
        # the histogram of the data
        n, bins, patches = plt.hist(coeff, 50, normed=1, facecolor=['b','r'][i], alpha=0.75, 
                                    histtype = 'step', label = graphName[i], linewidth=2.0
                                   )
        if (i == 0):
            plt.xlabel('Clust Coefficient')
            plt.ylabel('Count')
    plt.legend()
        #plt.axis([40, 160, 0, 0.03])
    plt.grid(True)
        #plt.savefig("../res/charac/cluster_Coefficient_distribution_{0}_graph_in_log_scale.png".format(graphName));
    plt.show()
        


# In[217]:


# Get the top 10 nodes with higest degree

random_un_nd = Get_nodes_degree(random_un)
train_un_nd = Get_nodes_degree(train_un)
# check the node id with highest degree
print random_un_nd[0]

random_hist_ud = Create_hist_nodes_degree(random_un)
train_hist_ud = Create_hist_nodes_degree(train_un)

random_betweeness = Get_nodes_betweenness(random_un)
train_betweeness = Get_nodes_betweenness(train_un)

random_ClusterCoeff = Get_nodes_ClusterCoefficient(random_un)
train_ClusterCoeff = Get_nodes_ClusterCoefficient(train_un)

# print top ten nodes in different aspect
print "Node_ID\tDegree of Nodes\tNode_ID\tClusterCoefficient\tNode_ID\tCentricity"
for i in range(10):
    print train_un_nd[i], train_ClusterCoeff[i], train_betweeness[i]



#print random_hist_ud[1]
#print train_hist_ud

a, b = np.polyfit(np.log10(train_hist_ud[0][1:len(train_hist_ud[0])]), np.log10(train_hist_ud[1][1:len(train_hist_ud[0])]), 1 )
print a, b

y_fit = 10 ** (a * np.log10(train_hist_ud[0]) + b )
plt.loglog(random_hist_ud[0], random_hist_ud[1], color = 'r', linestyle = 'dashed', label = 'random un directed graph')
plt.loglog(train_hist_ud[0], train_hist_ud[1], color = 'b', linestyle = 'dotted', label = 'train un directed graph')
plt.loglog(train_hist_ud[0], y_fit, color = 'g', label = 'fitted alpha =' + str(-a))
plt.xlabel('degree of nodes')
plt.ylabel('Count')
plt.legend()
plt.show()

plt.plot(random_hist_ud[0], random_hist_ud[1], color = 'r', linestyle = 'dashed', label = 'random un directed graph')
plt.plot(train_hist_ud[0], train_hist_ud[1], color = 'b', linestyle = 'dotted', label = 'train un directed graph')
plt.xlabel('degree of nodes')
plt.ylabel('Count')
plt.legend()
plt.show()

Graphs = [train_un, random_un]
print type(Graphs), type(Graphs[0]), len(Graphs)
plotClusterCoefficientDistribution_v2(Graphs, ['train network', 'random network'])






# In[220]:

graph_un = Get_nodes_degree(train_un);

beta = 0.5
delta = 0.5
SIR_out = []
for i in range(len(train_un_nd)):
#for i in range(6):
    nodes_I = set([graph_un[i][0]])
    infection_rate = []
    for j in range(100): # run each infection 50 times to get the mean
        infection_rate.append(SIR(train_un, nodes_I, beta, delta))

    #print i, train_un_nd[i][0], train_un_nd[i][1] , np.mean(infection_rate), np.std(infection_rate)
    SIR_out.append([i, graph_un[i][0], graph_un[i][1] , np.mean(infection_rate), np.std(infection_rate)])

print SIR_out





# In[224]:



x = [ i[2] for i in SIR_out]
y = [i[3] for i in SIR_out]
plt.scatter(x,y, label='SIR with infecton rate 0.5 and recovery rate 0.5 ')
plt.xlabel("degree of nodes")
plt.ylabel("Avg infection Rate")
plt.legend()
plt.show()


# In[230]:


tempx = [i for i in SIR_out if i[1] == 168]
tempy = [i[2] for i in SIR_out if i[1] == 168]
print tempx, tempy


#run_general_output(random_un, train_un)

