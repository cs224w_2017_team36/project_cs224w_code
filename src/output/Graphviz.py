
import snap

def plot_graph(graph,name):

    snap.DrawGViz(graph, snap.gvlNeato, name+".png", name)


Graph = snap.LoadEdgeList(snap.PUNGraph,"E:\Repository\Stanford\Project\data\Train_network_full_data_store_mf_simplified_11_12_reversedTopology.csv", 0, 1);
graphName = "ReversedTopology"

plot_graph(Graph,graphName)


Graph_regular = snap.LoadEdgeList(snap.PUNGraph,"E:\Repository\Stanford\Project\data\Train_network_full_data_store_mf_simplified_11_12_WithID.csv", 3, 4);
graphName = "normal"


plot_graph(Graph_regular,graphName)
