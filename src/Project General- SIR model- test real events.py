
# coding: utf-8

# In[105]:


import snap
#import matplotlib
#matplotlib.use('QT4Agg') #Problem with 64 bit installation, to be solved later
import matplotlib.pyplot as plt
import random
from scipy.stats import chi2_contingency
import numpy as np
import pandas as pd
import csv
import os

# You can set the path to the source file and run all def's from the bottom of the script
# In[231]:
def imitialize(path,column1,column2):

    # load the train network as undeirect graph
    graph_input_un = snap.LoadEdgeList(snap.PUNGraph, path , column1, column2)


# In[141]:


#    print graph_input_un.GetNodes()  # 252
#    print graph_input_un.GetEdges()  # 347
    graph_random_un = snap.GenRndGnm(snap.PUNGraph, graph_input_un.GetNodes(), graph_input_un.GetEdges())  # create random network with same # of nodes and edges


    return graph_random_un,graph_input_un
# In[214]:

def load_probs_infection(path):

    edge_inf_dict ={}

    with open(path, 'rb') as csvfile:
        next(csvfile, None)
        content = csv.reader(csvfile, delimiter=',')
        for row in content:
            edge_inf_dict[(int(row[0]),int(row[1]))] = float(row[2])
            edge_inf_dict[(int(row[1]), int(row[0]))] = float(row[2])
#            print ', '.join(row)

    return  edge_inf_dict

def load_probs_recovery(path):

    node_rec_dict = {}

    with open(path, 'rb') as csvfile:
        next(csvfile, None)
        content = csv.reader(csvfile, delimiter=',')
        for row in content:
            node_rec_dict[int(row[0])] = float(row[1])
#            print ', '.join(row)

    return node_rec_dict

def load_real_set(path_set):
    node_set = []

#    print path_set

    with open(path_set, 'rb') as csvfile:
        next(csvfile, None)
        content = csv.reader(csvfile)
        for row in content:
            node_set.append(int(row[0]))
            #            print ', '.join(row)

    return node_set

def load_routelist(path_list):
 #    print path_set

    routelist_dict = {}

    with open(path_list, 'rb') as csvfile:
        next(csvfile, None)
        content = csv.reader(csvfile, delimiter=',')
        for row in content:
#            print row[1].split(",")
            routelist_dict[int(row[0])] = list(row[1].split(","))
            #            print ', '.join(row)

    return  routelist_dict

def Get_nodes_degree(graph):
    """
    Function to return reverse ordering node_id, and node_degree 
    """
    DegToCntV = snap.TIntPrV()
    snap.GetNodeInDegV(graph, DegToCntV)
    DegToCntV_s = sorted(DegToCntV, key = lambda item: item.GetVal2(), reverse = True)
    return ([(item.GetVal1(), item.GetVal2()) for item in DegToCntV_s])

def SIR(Graph, nodes_I, beta, delta):
    """
    Function to perform the infection process using SIR model. 

    :param - Graph: snap.PUNGraph object representing an undirected graph
    :param - infaction rate and recovery rate
    :param - nodes_I, a set of nodes
    
    return: fraction of nodes infected 
    """

    nodes_all = []
    for NI in Graph.Nodes():
        nodes_all.append(NI.GetId())
    nodes_R = set([])
    nodes_S = set(nodes_all) - nodes_I

    while(len(nodes_I) !=0):

        if (len(nodes_I) ==len(nodes_all)):
            break
        S_s, I_s, J_s, R_s = [],[],[],[]
        for u in nodes_all:
            #print u, nodes_I
            if u in nodes_S:
                for NI in Graph.GetNI(u).GetOutEdges():
                    if (NI in nodes_I):
                        ran_number = random.random()
                        if ran_number < edge_infection[(u,NI)] and len(set(route_dict[u]).intersection(set(route_dict[NI]))) > 0:
                            S_s.append(u)
                            I_s.append(u)
                            break
            elif u in nodes_I:
                ran_number = random.random()
                #print "u in infected node", u, ran_number
                if ran_number <  delta:#node_infection[u]:
                    J_s.append(u)
                    R_s.append(u)
        nodes_S = nodes_S - set(S_s)
        nodes_I = (nodes_I | set(I_s)) - set(J_s)
        nodes_R = nodes_R | set(R_s)

#    print nodes_R
#    print real_nodeset
    jacc, perc_r_inreal = compute_jaccard_index(nodes_R, real_nodeset)

    return float(len(nodes_R))/len(nodes_all), nodes_R, jacc, perc_r_inreal


def simulate_node_iteration(graph, beta_in, delta_in,node_in, nr_tries):

    beta = beta_in
    delta = delta_in
    SIR_out = []
    jacc =[]
    perc_r_inreal =[]
    infected_set = []
    for i in range(nr_tries):
        nodes_I = set(node_in)
        infection_rate = []

        SIR_run = SIR(graph, nodes_I, beta, delta)

        infection_rate.append(SIR(graph, nodes_I, beta, delta)[0])
        infected_set.append(list(SIR(graph, nodes_I, beta, delta)[1]))
        jacc.append(SIR(graph, nodes_I, beta, delta)[2])
        perc_r_inreal.append(SIR(graph, nodes_I, beta, delta)[3])

        SIR_out.append([i, i, infection_rate, np.std(infection_rate)])

#    print jacc

    return SIR_out, infected_set, jacc, perc_r_inreal


def run_simulations(graph, beta_in, delta_in,node_list_in, nr_tries):

    title_text = 'SIR with avg. infecton prob. ' + str(beta_in) + ' and recovery rate ' + str(delta_in)

    v_s, v_alpha = 15, 0.5

    color_list = ['red', 'blue', 'black', 'green', 'yellow', 'purple', 'brown', 'grey', 'red', 'blue', 'black']

    for j in range(len(node_list_in)):
        node = 'Node ' + str(node_list_in[j][0])
        SIR_result, SIR_infectset, jacc_out, perc_r_inreal_out = simulate_node_iteration(graph, beta_avg, delta_in, set(node_list_in[j]), nr_tries)
        x, y = [i[1] for i in SIR_result], [i[2] for i in SIR_result]
        plt.scatter(x, y,  s=v_s,alpha=v_alpha,label=node )  #color=color_list[i],+ str(node_list_in[i][0])

    jac_mean = np.mean(jacc_out)
    perc_r_mean = np.mean(perc_r_inreal_out)
    SIR_infect_mean = np.mean([i[2] for i in SIR_result])
#    print SIR_infectset
#    SIR_infectset_jaccard = set(SIR_infectset)

#    print compute_jaccard_index(SIR_infectset_jaccard,real_nodeset)

    # plt.xlabel("Event")
    # plt.ylabel("Infection proportion")
    # plt.axis((0, nr_tries, 0, 1))
    # plt.title(title_text)


    # plt.legend()
    # plt.show()

    return jac_mean,perc_r_mean,SIR_infect_mean



def compute_jaccard_index(set_1, set_2):
    n = len(set_1.intersection(set_2))
    return n / float(len(set_1) + len(set_2) - n),  n / float(len(set_2))

path_source_file = 'E:\Repository\Stanford\Project\data\Train_network_full_data_store_mf_simplified_12_01_WithID_2.csv'

random_un,train_un = imitialize(path_source_file,3,4)

global edge_infection,node_infection,real_nodeset, route_dict

path_edge_infection = 'E:\Repository\Stanford\Project\data\Edgelist_probability_input.csv'
edge_infection = load_probs_infection(path_edge_infection)

path_node_recovery = 'E:\Repository\Stanford\Project\data\Node_recovery_input.csv'
node_infection = load_probs_recovery(path_node_recovery)

beta_avg = "{0:.2f}".format(float(sum(edge_infection.values())) / len(edge_infection))
delta_avg = "{0:.2f}".format(float(sum(node_infection.values())) / len(node_infection))

#simulate_allnodes(train_un,beta_avg,delta_avg)

#node_in = '13'

#simulate_node_iteration(train_un,beta_avg,0.75,[320],100)

#simulate_node_iteration(train_un,beta_avg,delta_avg,205,100)

real_set_sources = ['identify_delay_events_set_7_02-05-2017.csv','identify_delay_events_set_24_02-05-2017.csv','identify_delay_events_set_24_06-04-2017.csv','identify_delay_events_set_24_13-04-2017.csv','identify_delay_events_set_32_16-05-2017.csv','identify_delay_events_set_87_02-05-2017.csv','identify_delay_events_set_94_02-05-2017.csv','identify_delay_events_set_128_24-05-2017.csv','identify_delay_events_set_171_05-04-2017.csv','identify_delay_events_set_263_13-04-2017.csv','identify_delay_events_set_279_06-04-2017.csv','identify_delay_events_set_279_12-05-2017.csv','identify_delay_events_set_279_28-04-2017.csv','identify_delay_events_set_304_23-05-2017.csv','identify_delay_events_set_356_03-04-2017.csv']

real_set_nodes =[[7],[24],[24],[24],[32],[87],[94],[128],[171],[263],[279],[279],[279],[304],[356]]

routelist_path = 'E:\Repository\Stanford\Project\data\Node_routes.csv'
route_dict = load_routelist(routelist_path)
#print len(real_set_sources),len(real_set_nodes)

for i in range(len(real_set_sources)):
    real_set_path = os.path.join('E:\Repository\Stanford\Project\data\sets',real_set_sources[i])
    node_in = real_set_nodes[i]



    real_nodeset = set(load_real_set(real_set_path))
    jac_mean, perc_r_mean, SIR_infect_mean = run_simulations(train_un,beta_avg,0.5,[node_in],50)

    infection_realnodes = len(real_nodeset) / float(train_un.GetNodes())

#    print ""
    print node_in[0], "{0:.3f}".format(infection_realnodes), "{0:.3f}".format(SIR_infect_mean), "{0:.3f}".format(jac_mean), "{0:.3f}".format(perc_r_mean)
#print real_nodeset




#print route_dict
#print len(set(route_dict[1]).intersection(set(route_dict[2])))>0

#run_simulations(train_un,beta_avg,[[7]],0.75,50)

