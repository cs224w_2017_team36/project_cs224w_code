import snap
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt


def plotDegreeDistribution(Graph, graphName):
	X, Y = [], []
	N = Graph.GetNodes();
	c1=snap.TIntPrV();
	snap.GetDegCnt(Graph,c1);
	for i in c1:
		X.append(i.GetVal1());
		Y.append(i.GetVal2());
	############################################################################
	plt.plot(X, Y)
	plt.xlabel('Node Degree')
	plt.ylabel('Proportion of Nodes with a Given Degree')
	plt.savefig("../res/charac/degreeDistribution_{0}_graph.png".format(graphName));
	plt.show()
	plt.figure()
	plt.loglog(X, Y, linestyle = 'dotted', color = 'b')
	plt.xlabel('Node Degree (log)')
	plt.ylabel('Count of Nodes with a Given Degree (log)')
	plt.savefig("../res/charac/degreeDistribution_{0}_graph_in_log_scale.png".format(graphName));
	plt.show()

def plotClusterCoefficientDistribution(Graph, graphName):
	coeff = []
	for node in Graph.Nodes():
		coeff.append(snap.GetNodeClustCf(Graph, node.GetId()));
	# the histogram of the data
	n, bins, patches = plt.hist(coeff, 50, normed=1, facecolor='green', alpha=0.75)
	plt.xlabel('Clust Coefficient')
	plt.ylabel('Count')
	plt.title("clust_Coefficients_Distribution_for_{0}".format(graphName))
	#plt.axis([40, 160, 0, 0.03])
	plt.grid(True)
	plt.savefig("../res/charac/cluster_Coefficient_distribution_{0}_graph_in_log_scale.png".format(graphName));
	plt.show()
		
		
		
train_un_old = snap.LoadEdgeList(snap.PNGraph, "../data/Train_network_full_data_store_mf_simplified_11_12_WithID.csv", 3, 4);
train_un = snap.LoadEdgeList(snap.PNGraph, "../data/Train_network_full_data_store_mf_simplified_12_01_WithID_2.csv", 3, 4);


print train_un.GetNodes()  # 163
print train_un.GetEdges()  # 180
print train_un_old.GetNodes()  # 251
print train_un_old.GetEdges()  #352
## degree distribution:
graphName = "normal"
plotDegreeDistribution(Graph, graphName);
plotClusterCoefficientDistribution(Graph, graphName)

Graph = snap.LoadEdgeList(snap.PUNGraph, "../data/Train_network_full_data_store_mf_simplified_11_12_reversedTopology.csv", 0, 1);
graphName = "ReversedTopology"
plotDegreeDistribution(Graph, graphName);
plotClusterCoefficientDistribution(Graph, graphName)




