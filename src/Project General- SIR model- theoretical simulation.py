
# coding: utf-8

# In[105]:


import snap
#import matplotlib
#matplotlib.use('QT4Agg') #Problem with 64 bit installation, to be solved later
import matplotlib.pyplot as plt
import random
from scipy.stats import chi2_contingency
import numpy as np
import pandas as pd
import csv


# You can set the path to the source file and run all def's from the bottom of the script
# In[231]:
def imitialize(path,column1,column2):

    # load the train network as undeirect graph
    graph_input_un = snap.LoadEdgeList(snap.PUNGraph, path , column1, column2)


# In[141]:


#    print graph_input_un.GetNodes()  # 252
#    print graph_input_un.GetEdges()  # 347
    graph_random_un = snap.GenRndGnm(snap.PUNGraph, graph_input_un.GetNodes(), graph_input_un.GetEdges())  # create random network with same # of nodes and edges


    return graph_random_un,graph_input_un
# In[214]:

def load_probs_infection(path):

    edge_inf_dict ={}

    with open(path, 'rb') as csvfile:
        next(csvfile, None)
        content = csv.reader(csvfile, delimiter=',')
        for row in content:
            edge_inf_dict[(int(row[0]),int(row[1]))] = float(row[2])
            edge_inf_dict[(int(row[1]), int(row[0]))] = float(row[2])
#            print ', '.join(row)

    return  edge_inf_dict

def load_probs_recovery(path):

    node_rec_dict = {}

    with open(path, 'rb') as csvfile:
        next(csvfile, None)
        content = csv.reader(csvfile, delimiter=',')
        for row in content:
            node_rec_dict[int(row[0])] = float(row[1])
#            print ', '.join(row)

    return node_rec_dict

def Get_nodes_degree(graph):
    """
    Function to return reverse ordering node_id, and node_degree 
    """
    DegToCntV = snap.TIntPrV()
    snap.GetNodeInDegV(graph, DegToCntV)
    DegToCntV_s = sorted(DegToCntV, key = lambda item: item.GetVal2(), reverse = True)
    return ([(item.GetVal1(), item.GetVal2()) for item in DegToCntV_s])

def load_routelist(path_list):
 #    print path_set

    routelist_dict = {}

    with open(path_list, 'rb') as csvfile:
        next(csvfile, None)
        content = csv.reader(csvfile, delimiter=',')
        for row in content:
#            print row[1].split(",")
            routelist_dict[int(row[0])] = list(row[1].split(","))
            #            print ', '.join(row)

    return  routelist_dict

def SIR(Graph, nodes_I, beta, delta):
    """
    Function to perform the infection process using SIR model. 

    :param - Graph: snap.PUNGraph object representing an undirected graph
    :param - infaction rate and recovery rate
    :param - nodes_I, a set of nodes
    
    return: fraction of nodes infected 
    """

    nodes_all = []
    for NI in Graph.Nodes():
        nodes_all.append(NI.GetId())
    nodes_R = set([])
    nodes_S = set(nodes_all) - nodes_I
    #print "Initial infected ", nodes_I
    
    while(len(nodes_I) !=0):
        #print "infected nodes are ", len(nodes_I)
        #print nodes_I
        if (len(nodes_I) ==len(nodes_all)):
            break
        S_s, I_s, J_s, R_s = [],[],[],[]
        for u in nodes_all:
            #print u, nodes_I
            if u in nodes_S:
                for NI in Graph.GetNI(u).GetOutEdges():
                    if (NI in nodes_I):
                        ran_number = random.random()
                        if ran_number < edge_infection[(u,NI)] and len(set(route_dict[u]).intersection(set(route_dict[NI]))) > 0:
                            S_s.append(u)
                            I_s.append(u)
                            break
            elif u in nodes_I:
                ran_number = random.random()
                #print "u in infected node", u, ran_number
                if ran_number < delta:#node_infection[u]:
                    J_s.append(u)
                    R_s.append(u)
        nodes_S = nodes_S - set(S_s)
        nodes_I = (nodes_I | set(I_s)) - set(J_s)
        nodes_R = nodes_R | set(R_s)
    
    return float(len(nodes_R))/len(nodes_all), nodes_R

def simulate_node_iteration(graph, beta_in, delta_in,node_in, nr_tries):

    beta = beta_in
    delta = delta_in
    SIR_out = []
    infected_set = []
    for i in range(nr_tries):
        nodes_I = set(node_in)
        infection_rate = []

        infection_rate.append(SIR(graph, nodes_I, beta, delta)[0])
        infected_set.append(list(SIR(graph, nodes_I, beta, delta)[1]))

        SIR_out.append([i, i, infection_rate, np.std(infection_rate)])

#    print SIR_out

    return SIR_out, infected_set

def run_simulations(graph, beta_in,node_list_in, delta_in, nr_tries):

    title_text = 'SIR with avg. infecton prob. ' + str(beta_in) + ' and recovery rate ' + str(delta_in)

    v_s, v_alpha = 15, 0.5

    color_list = ['red', 'blue', 'black', 'green', 'yellow', 'purple', 'brown', 'grey', 'red', 'blue', 'black']

    for i in range(len(node_list_in)):
        node = 'Node ' + str(node_list_in[i][0])
        SIR_result, SIR_infectset = simulate_node_iteration(graph, beta_avg, 0.5, set(node_list_in[i]), nr_tries)
        x, y = [i[1] for i in SIR_result], [i[2] for i in SIR_result]
        plt.scatter(x, y,  s=v_s,alpha=v_alpha,label=node )  #color=color_list[i],+ str(node_list_in[i][0])

        file = node + '.csv'
        with open(file, 'wb') as myfile:
            wr = csv.writer(myfile)
            for i in SIR_infectset:
                wr.writerow(i)

    plt.xlabel("Event")
    plt.ylabel("Infection proportion")
    plt.axis((0, nr_tries, 0, 1))
    plt.title(title_text)

#
    plt.legend()
    plt.show()

def compute_jaccard_index(set_1, set_2):
    n = len(set_1.intersection(set_2))
    return n / float(len(set_1) + len(set_2) - n)

path_source_file = 'E:\Repository\Stanford\Project\data\Train_network_full_data_store_mf_simplified_12_01_WithID_2.csv'

random_un,train_un = imitialize(path_source_file,3,4)

global edge_infection,node_infection

path_edge_infection = 'E:\Repository\Stanford\Project\data\Edgelist_probability_input.csv'
edge_infection = load_probs_infection(path_edge_infection)

path_node_recovery = 'E:\Repository\Stanford\Project\data\Node_recovery_input.csv'
node_infection = load_probs_recovery(path_node_recovery)

beta_avg = "{0:.2f}".format(float(sum(edge_infection.values())) / len(edge_infection))
delta_avg = "{0:.2f}".format(float(sum(node_infection.values())) / len(node_infection))

routelist_path = 'E:\Repository\Stanford\Project\data\Node_routes.csv'
route_dict = load_routelist(routelist_path)
#simulate_allnodes(train_un,beta_avg,delta_avg)

#node_in = '13'

#simulate_node_iteration(train_un,beta_avg,0.75,[320],100)

#simulate_node_iteration(train_un,beta_avg,delta_avg,205,100)

node_list = [[304],[11],[171],[24],[30]]
#node_list = [[325],[372],[252],[677],[320]]

run_simulations(train_un,beta_avg,node_list,0.5,50)

